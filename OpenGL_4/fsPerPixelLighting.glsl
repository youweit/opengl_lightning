//#define NPR
#version 120
varying vec3 fN;

varying vec3 Light0fL; // 輸出 Light Direction 在鏡頭座標下的方向
varying vec3 Light1fL;
varying vec3 Light2fL;

varying vec3 fE;

// 以下為新增的部分
uniform vec4  LightInView;        // Light's position in View Space
uniform vec4  AmbientProduct;  // light's ambient  與 Object's ambient  與 ka 的乘積
uniform vec4  DiffuseProduct;  // light's diffuse  與 Object's diffuse  與 kd 的乘積
uniform vec4  SpecularProduct; // light's specular 與 Object's specular 與 ks 的乘積
uniform float fShininess;

uniform int   iLightingR;
uniform int   iLightingG;
uniform int   iLightingB;

uniform vec4  vObjectColor;    // 代表物件的單一顏色



vec4 light0()
{
	// 先宣告 diffuse 與 specular
	vec4 mcolor = vec4(vObjectColor.x,0.0,0.0,1.0);
    vec4 diffuse = vec4(0.0,0.0,0.0,1.0);
	vec4 specular = vec4(0.0,0.0,0.0,1.0);
	
		// 1. 計算 Ambient color : Ia = AmbientProduct = Ka * Material.ambient * La = 
		vec4 ambient = AmbientProduct * mcolor; // m_sMaterial.ka * m_sMaterial.ambient * vLightI;

		// 單位化傳入的 Normal Dir
		vec3 vN = normalize(fN); 

		// 2. 單位化傳入的 Light Dir
		vec3 vL = normalize(Light0fL); // normalize light vector

		// 5. 計算 L dot N
		float fLdotN = vL.x*vN.x + vL.y*vN.y + vL.z*vN.z;
		if( fLdotN >= 0 ) { // 該點被光源照到才需要計算
			
			// Diffuse Color : Id = Kd * Material.diffuse * Ld * (L dot N)
			diffuse = fLdotN * DiffuseProduct * mcolor; 
			if (dot(normalize(vec3(0,-1,0)),-vL)> 0.7) {

				vec3 vV = normalize(fE);
				vec3 vRefL = normalize(2.0f * (fLdotN) * vN - vL);

				//   計算  vReflectedL dot View
			    float RdotV = vRefL.x*vV.x + vRefL.y*vV.y + vRefL.z*vV.z;
			    // Specular Color : Is = Ks * Material.specular * Ls * (R dot V)^Shininess;
			    if( RdotV > 0 ) specular = SpecularProduct * pow(RdotV, fShininess)*mcolor; 
				return vec4((ambient + diffuse + specular).xyz, 1.0);
			}
			
		}
		
		return vec4(0,0,0, 1.0);
	
}

vec4 light1()
{
	// 先宣告 diffuse 與 specular
	vec4 mcolor = vec4(0.0,vObjectColor.y,0.0,1.0);
    vec4 diffuse = vec4(0.0,0.0,0.0,1.0);
	vec4 specular = vec4(0.0,0.0,0.0,1.0);
		// 1. 計算 Ambient color : Ia = AmbientProduct = Ka * Material.ambient * La = 
		vec4 ambient = AmbientProduct*mcolor; // m_sMaterial.ka * m_sMaterial.ambient * vLightI;

		// 單位化傳入的 Normal Dir
		vec3 vN = normalize(fN); 

		// 2. 單位化傳入的 Light Dir
		vec3 vL = normalize(Light1fL); // normalize light vector

		// 5. 計算 L dot N
		float fLdotN = vL.x*vN.x + vL.y*vN.y + vL.z*vN.z;
		if( fLdotN >= 0 ) { // 該點被光源照到才需要計算

#ifndef NPR
			// Diffuse Color : Id = Kd * Material.diffuse * Ld * (L dot N)
			diffuse = fLdotN * DiffuseProduct* mcolor; 
#else
			if( fLdotN >= 0.85 ) diffuse = 1.0 * DiffuseProduct* mcolor;
			else if( fLdotN >= 0.55 ) diffuse = 0.55 * DiffuseProduct* mcolor;
			else diffuse = 0.35 * DiffuseProduct* mcolor;
#endif

			// Specular color
			// Method 1: Phone Model
			// 計算 View Vector
			// 單位化傳入的 fE , View Direction
			vec3 vV = normalize(fE);

			//計算 Light 的 反射角 vRefL = 2 * fLdotN * vN - L
			// 同時利用 normalize 轉成單位向量
			vec3 vRefL = normalize(2.0f * (fLdotN) * vN - vL);

			//   計算  vReflectedL dot View
			float RdotV = vRefL.x*vV.x + vRefL.y*vV.y + vRefL.z*vV.z;

#ifndef NPR
			// Specular Color : Is = Ks * Material.specular * Ls * (R dot V)^Shininess;
			if( RdotV > 0 ) specular = SpecularProduct * pow(RdotV, fShininess) * mcolor; 
#else
			specular = vec4(0.0,0.0,0.0,1.0); 
#endif
		}
		return vec4((ambient + diffuse + specular).xyz, 1.0);
}


vec4 light2()
{
	// 先宣告 diffuse 與 specular
	vec4 mcolor = vec4(0.0,0.0,vObjectColor.z,1.0);
    vec4 diffuse = vec4(0.0,0.0,0.0,1.0);
	vec4 specular = vec4(0.0,0.0,0.0,1.0);
	
		// 1. 計算 Ambient color : Ia = AmbientProduct = Ka * Material.ambient * La = 
		vec4 ambient = AmbientProduct*mcolor; // m_sMaterial.ka * m_sMaterial.ambient * vLightI;

		// 單位化傳入的 Normal Dir
		vec3 vN = normalize(fN); 

		// 2. 單位化傳入的 Light Dir
		vec3 vL = normalize(Light2fL); // normalize light vector

		// 5. 計算 L dot N
		float fLdotN = vL.x*vN.x + vL.y*vN.y + vL.z*vN.z;
		if( fLdotN >= 0 ) { // 該點被光源照到才需要計算

#ifndef NPR
			// Diffuse Color : Id = Kd * Material.diffuse * Ld * (L dot N)
			diffuse = fLdotN * DiffuseProduct * mcolor; 
#else
			if( fLdotN >= 0.85 ) diffuse = 1.0 * DiffuseProduct * mcolor;
			else if( fLdotN >= 0.55 ) diffuse = 0.55 * DiffuseProduct * mcolor;
			else diffuse = 0.35 * DiffuseProduct * mcolor;
#endif

			// Specular color
			// Method 1: Phone Model
			// 計算 View Vector
			// 單位化傳入的 fE , View Direction
			vec3 vV = normalize(fE);

			//計算 Light 的 反射角 vRefL = 2 * fLdotN * vN - L
			// 同時利用 normalize 轉成單位向量
			vec3 vRefL = normalize(2.0f * (fLdotN) * vN - vL);

			//   計算  vReflectedL dot View
			float RdotV = vRefL.x*vV.x + vRefL.y*vV.y + vRefL.z*vV.z;

#ifndef NPR
			// Specular Color : Is = Ks * Material.specular * Ls * (R dot V)^Shininess;
			if( RdotV > 0 ) specular = SpecularProduct * pow(RdotV, fShininess) * mcolor; 
#else
			specular = vec4(0.0,0.0,0.0,1.0); 
#endif
		}
		return vec4((ambient + diffuse + specular).xyz, 1.0);	
}

void main()
{
	vec4 final = vec4(0.0,0.0,0.0,1.0);
	if(iLightingR == 1)  final+=light0();
	if(iLightingG == 1)  final+=light1();
	if(iLightingB == 1)  final+=light2();

	gl_FragColor = final;
}