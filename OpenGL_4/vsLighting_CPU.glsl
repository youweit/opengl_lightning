
#version 120
attribute vec4 vPosition;
attribute vec3 vNormal;
attribute vec4 vVtxColor;
varying vec4 vColor;

uniform mat4 ModelView;
uniform mat4 Projection;
uniform vec4 vObjectColor;

void main()
{
	gl_Position = Projection * ModelView * vPosition;
	if( vVtxColor.x == -1.0f && vVtxColor.y == -1.0f && vVtxColor.z == -1.0f ) {
		vColor = vObjectColor;
	}
	else vColor = vVtxColor;
}