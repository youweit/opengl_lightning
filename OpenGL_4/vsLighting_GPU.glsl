// Phong reflection model
// #define BLINN_PHONG
#version 120
#define M_PI 3.14159265358979

attribute  vec4  vPosition;	// Vertex Position
attribute  vec3  vNormal;    // Vertex Normal
attribute  vec4  vVtxColor;  // Vertex Color
varying vec4  vColor;     // 輸出的顏色

uniform mat4  ModelView;   // Model View Matrix
uniform mat4  Projection;  // Projection Matrix
uniform vec4  vObjectColor;    // 代表物件的單一顏色

// 上面的都與  vsLighting_CPU.glsl 相同
// 以下為新增的部分
uniform vec4  LightInView;        // Light's position in View Space
uniform vec4  AmbientProduct;  // light's ambient  與 Object's ambient  與 ka 的乘積
uniform vec4  DiffuseProduct;  // light's diffuse  與 Object's diffuse  與 kd 的乘積
uniform vec4  SpecularProduct; // light's specular 與 Object's specular 與 ks 的乘積
uniform float fShininess;
uniform int   iLighting;
uniform int   iLightNum;
float att,spotEffect;

vec4 light0()
{

	vec4 mcolor = vec4(vObjectColor.x,0.0,0.0,1.0);
	vec4 diffuse = vec4(0.0,0.0,0.0,1.0);
	vec4 specular = vec4(0.0,0.0,0.0,1.0);


	if( iLighting != 1) {
		gl_Position = Projection * ModelView * vPosition;	// 計算 vPosition 投影後的位置
		vColor = vObjectColor;
	}
	else {	
		// 1. 將點頂轉到鏡頭座標系統，必須以 m_mxMVFinal 計算在世界座標的絕對位置
		vec4 vPosInView = ModelView * vPosition;  

		// 2. 將面的 Normal 轉到鏡頭座標系統，並轉成單位向量
		// 最後一個補上 0 ，不需要位移量，否則出來的 Normal 會不對
		// 同時利用 normalize 轉成單位向量

        // 以下兩行用於計算對物件進行 non-uniform scale 時，物件 normal 的修正計算
		//mat3 ITModelView = transpose(inverse(mat3(ModelView)); 
		//vec3 vN = normalize(ITModelView * vNormal); 
		vec3 vN = normalize(ModelView * vec4(vNormal, 0.0)).xyz; 


		//-------------------------------------------------------------------------

		// 3. 計算 Ambient color : Ia = AmbientProduct = Ka * Material.ambient * La = 
		vec4 ambient = AmbientProduct*mcolor; // m_sMaterial.ka * m_sMaterial.ambient * vLightI;

		// 4. 傳入的 Light 的位置已經在鏡頭座標
		vec3 vL = normalize(vec3(LightInView.xyz - vPosInView.xyz)); // normalize light vector

		// 5. 計算 L dot N
		float fLdotN = vL.x*vN.x + vL.y*vN.y + vL.z*vN.z;
		if( fLdotN >= 0 ) { // 該點被光源照到才需要計算
			// Diffuse Color : Id = Kd * Material.diffuse * Ld * (L dot N)
			diffuse = fLdotN * DiffuseProduct * mcolor; 

			// Specular color
			// Method 1: Phone Model
			// 計算 View Vector
			// 目前已經以鏡頭座標為基礎, 所以 View 的位置就是 (0,0,0), 所以點位置的負向就是 View Dir
			vec3 vV = -normalize(vPosInView.xyz);

#ifndef BLINN_PHONG
			//計算 Light 的 反射角 vRefL = 2 * fLdotN * vN - L
			// 同時利用 normalize 轉成單位向量
			vec3 vRefL = normalize(2.0f * (fLdotN) * vN - vL);

			//   計算  vReflectedL dot View
			float RdotV = vRefL.x*vV.x + vRefL.y*vV.y + vRefL.z*vV.z;

			// Specular Color : Is = Ks * Material.specular * Ls * (R dot V)^Shininess;
			if( RdotV > 0 ) specular = SpecularProduct * pow(RdotV, fShininess) * mcolor; 
#else

			// Blinn-Phong Reflection Model
			vec3 vH = normalize( vL + vV ); 
			float HdotN = vH.x*vN.x + vH.y*vN.y + vH.z*vN.z; //   計算  H dot N
			if( HdotN > 0 ) specular = SpecularProduct * pow(HdotN, fShininess) * mcolor; 
#endif
		}


		
		//gl_Position = Projection * vPosInView;
		return vec4((ambient + diffuse + specular).xyz, 1.0);
		// vColor = vec4((ambient + diffuse + specular).xyz, 1.0);
	}
}



vec4 light1()
{
	vec4 mcolor = vec4(0.0,vObjectColor.y,0.0,1.0);
	vec4 diffuse = vec4(0.0,0.0,0.0,1.0)*mcolor;
	vec4 specular = vec4(0.0,0.0,0.0,1.0)*mcolor;
	vec3 testp = vec3(LightInView.x+5,LightInView.y+5,LightInView.z+5); 
	//vec4 testp = vec4(3.0,3.0,0.0,1.0);
	if( iLighting != 1) {
		gl_Position = Projection * ModelView * vPosition;	// 計算 vPosition 投影後的位置
		vColor = vObjectColor;
	}
	else {	
		// 1. 將點頂轉到鏡頭座標系統，必須以 m_mxMVFinal 計算在世界座標的絕對位置
		vec4 vPosInView = ModelView * vPosition;  

		// 2. 將面的 Normal 轉到鏡頭座標系統，並轉成單位向量
		// 最後一個補上 0 ，不需要位移量，否則出來的 Normal 會不對
		// 同時利用 normalize 轉成單位向量

        // 以下兩行用於計算對物件進行 non-uniform scale 時，物件 normal 的修正計算
		//mat3 ITModelView = transpose(inverse(mat3(ModelView)); 
		//vec3 vN = normalize(ITModelView * vNormal); 
		vec3 vN = normalize(ModelView * vec4(vNormal, 0.0)).xyz; 


		//-------------------------------------------------------------------------

		// 3. 計算 Ambient color : Ia = AmbientProduct = Ka * Material.ambient * La = 
		vec4 ambient = AmbientProduct * mcolor; // m_sMaterial.ka * m_sMaterial.ambient * vLightI;

		// 4. 傳入的 Light 的位置已經在鏡頭座標
		vec3 vL = normalize(vec3(testp.xyz - vPosInView.xyz)); // normalize light vector

		// 5. 計算 L dot N
		float fLdotN = vL.x*vN.x + vL.y*vN.y + vL.z*vN.z;
		if( fLdotN >= 0 ) { // 該點被光源照到才需要計算
			// Diffuse Color : Id = Kd * Material.diffuse * Ld * (L dot N)
			diffuse = fLdotN * DiffuseProduct *mcolor; 

			// Specular color
			// Method 1: Phone Model
			// 計算 View Vector
			// 目前已經以鏡頭座標為基礎, 所以 View 的位置就是 (0,0,0), 所以點位置的負向就是 View Dir
			vec3 vV = -normalize(vPosInView.xyz);

#ifndef BLINN_PHONG
			//計算 Light 的 反射角 vRefL = 2 * fLdotN * vN - L
			// 同時利用 normalize 轉成單位向量
			vec3 vRefL = normalize(2.0f * (fLdotN) * vN - vL);

			//   計算  vReflectedL dot View
			float RdotV = vRefL.x*vV.x + vRefL.y*vV.y + vRefL.z*vV.z;

			// Specular Color : Is = Ks * Material.specular * Ls * (R dot V)^Shininess;
			if( RdotV > 0 ) specular = SpecularProduct * pow(RdotV, fShininess) *mcolor; 
#else

			// Blinn-Phong Reflection Model
			vec3 vH = normalize( vL + vV ); 
			float HdotN = vH.x*vN.x + vH.y*vN.y + vH.z*vN.z; //   計算  H dot N
			if( HdotN > 0 ) specular = SpecularProduct * pow(HdotN, fShininess) *mcolor; 
#endif
		}


		
		gl_Position = Projection * vPosInView;
		return vec4((ambient + diffuse + specular).xyz, 1.0);
		// vColor = vec4((ambient + diffuse + specular).xyz, 1.0);
	}
}

vec4 light2()
{
	vec4 mcolor = vec4(0.0,0.0,vObjectColor.z,1.0);
	vec4 diffuse = vec4(0.0,0.0,0.0,1.0)*mcolor;
	vec4 specular = vec4(0.0,0.0,0.0,1.0)*mcolor;
	vec3 testp = vec3(LightInView.x-3,LightInView.y,LightInView.x-3); 
	//vec4 testp = vec4(1.0,7.0,0.0,1.0);
	
	if( iLighting != 1) {
		gl_Position = Projection * ModelView * vPosition;	// 計算 vPosition 投影後的位置
		vColor = vObjectColor;
	}
	else {	
		// 1. 將點頂轉到鏡頭座標系統，必須以 m_mxMVFinal 計算在世界座標的絕對位置
		vec4 vPosInView = ModelView * vPosition;  

		// 2. 將面的 Normal 轉到鏡頭座標系統，並轉成單位向量
		// 最後一個補上 0 ，不需要位移量，否則出來的 Normal 會不對
		// 同時利用 normalize 轉成單位向量

        // 以下兩行用於計算對物件進行 non-uniform scale 時，物件 normal 的修正計算
		//mat3 ITModelView = transpose(inverse(mat3(ModelView)); 
		//vec3 vN = normalize(ITModelView * vNormal); 
		vec3 vN = normalize(ModelView * vec4(vNormal, 0.0)).xyz; 


		//-------------------------------------------------------------------------

		// 3. 計算 Ambient color : Ia = AmbientProduct = Ka * Material.ambient * La = 
		vec4 ambient = AmbientProduct * mcolor; // m_sMaterial.ka * m_sMaterial.ambient * vLightI;

		// 4. 傳入的 Light 的位置已經在鏡頭座標
		vec3 vL = normalize(vec3(testp.xyz - vPosInView.xyz)); // normalize light vector

		// 5. 計算 L dot N
		float fLdotN = vL.x*vN.x + vL.y*vN.y + vL.z*vN.z;
		if( fLdotN >= 0 ) { // 該點被光源照到才需要計算
			// Diffuse Color : Id = Kd * Material.diffuse * Ld * (L dot N)
			diffuse = fLdotN * DiffuseProduct *mcolor; 

			// Specular color
			// Method 1: Phone Model
			// 計算 View Vector
			// 目前已經以鏡頭座標為基礎, 所以 View 的位置就是 (0,0,0), 所以點位置的負向就是 View Dir
			vec3 vV = -normalize(vPosInView.xyz);

#ifndef BLINN_PHONG
			//計算 Light 的 反射角 vRefL = 2 * fLdotN * vN - L
			// 同時利用 normalize 轉成單位向量
			vec3 vRefL = normalize(2.0f * (fLdotN) * vN - vL);

			//   計算  vReflectedL dot View
			float RdotV = vRefL.x*vV.x + vRefL.y*vV.y + vRefL.z*vV.z;

			// Specular Color : Is = Ks * Material.specular * Ls * (R dot V)^Shininess;
			if( RdotV > 0 ) specular = SpecularProduct * pow(RdotV, fShininess) *mcolor; 
#else

			// Blinn-Phong Reflection Model
			vec3 vH = normalize( vL + vV ); 
			float HdotN = vH.x*vN.x + vH.y*vN.y + vH.z*vN.z; //   計算  H dot N
			if( HdotN > 0 ) specular = SpecularProduct * pow(HdotN, fShininess) *mcolor; 
#endif
		}


		
		gl_Position = Projection * vPosInView;
		return vec4((ambient + diffuse + specular).xyz, 1.0);
		// vColor = vec4((ambient + diffuse + specular).xyz, 1.0);
	}
}
void main()
{
	vec4 final = vec4(0.0,0.0,0.0,0.0);
		// 計算 vPosition 投影後的位置
	final += light0();
	final += light1();
	final += light2();

	vColor = final;
}

