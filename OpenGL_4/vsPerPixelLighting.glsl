// Phong reflection model
#version 120
attribute vec4 vPosition;	  // Vertex Position
attribute vec3 vNormal;    // Vertex Normal
attribute vec4 vVtxColor;  // Vertex Color

varying vec3 fN;// 輸出 Normal 在鏡頭座標下的方向

varying vec3 Light0fL; // 輸出 Light Direction 在鏡頭座標下的方向
varying vec3 Light1fL;
varying vec3 Light2fL;

varying vec3 fE;// 輸出 View Direction 在鏡頭座標下的方向

uniform mat4  ModelView;   // Model View Matrix
uniform mat4  Projection;  // Projection Matrix
uniform vec4  LightInView; // Light's position in View Space

void main()
{
	vec4 vPosInView = ModelView * vPosition;
	// 目前已經以鏡頭座標為基礎, 所以 View 的位置就是 (0,0,0), 所以位置的富項就是 View Dir
	fE = -vPosInView.xyz;

	// 以下兩行用於計算對物件進行 non-uniform scale 時，物件 normal 的修正計算
	//		mat3 ITModelView = transpose(inverse(mat3(ModelView)); 
	//		vec3 vN = normalize(ITModelView * vNormal); 
	fN = (ModelView * vec4(vNormal, 0.0)).xyz;

	Light0fL = vec3(LightInView.xyz - vPosInView.xyz);
	Light1fL = vec3(vec3(LightInView.x,LightInView.y,LightInView.z) - vPosInView.xyz);
	Light2fL = vec3(vec3(LightInView.x,LightInView.y,LightInView.z) - vPosInView.xyz);

	gl_Position = Projection * vPosInView;
}
